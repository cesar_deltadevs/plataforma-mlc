﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Usuarios</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <link href="<?= ($ruta) ?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/deltadev.css" rel="stylesheet" />
</head>
<body id="usuario">

    <!----------------------EN ESTA PARTE SE PONDRA EL MENU------------------------->

    <nav class="navbar navbar-expand-lg" style="background-color: black;">
        <p class="navbar-brand letraBlanca"><span><img src="<?= ($ruta) ?>img/LOGOPHOTONEWS1.png" style="width:20%"/></span>Bienvenido Usuario</p>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#"><span class="sr-only"></span></a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <button type="button" class="btn btn-danger espacio" onclick="location.href='<?= ($ruta) ?>';" id="cerrarSesion" type="submit"> Cerrar Sesion</button>
            </form>
        </div>
    </nav>

    <!------------------------------------------------------------------------------->
    <br />
    <div class="container">
        <!----------------------EN ESTA PARTE SE PONDRA LA IMAGEN DE PERFIL E INFORMACION DEL USUARIO------------------------->
        <div class="row">
            <div class="col-md-3">



                <!---------------------------Aqui ira la imagen del usuario---------------------------------------->
                <img src="<?= ($ruta) ?>img/usuario.png.png" class="rounded float-left" alt="Responsive image" style="width:117.5%;"/>

                <!------------------------------------------------------------------------------------------------->
            </div>
            <div class="col-md-6" style="background-color:#e9e4e3; padding:1em; margin:0em 3em 0em 3em;">
                <!---------------------------Aqui ira la informacion del usuario---------------------------------------->

                <h5> Nombre: Maria </h5>
                <h5> Apellido Paterno: Langarica </h5>
                <h5> Apellido Materno: Correa  </h5>
                <h5> sexo: Mujer  </h5>
                <h5> Domicilio: Balcón de los edecanes- Tlalnepantla, Edo.  </h5>
                <h5> C.P: 56790 </h5>
                <h5> Telefono: 55 16-20-23-56  </h5>
                <h5> Correo: maria_langarica@gmail.com </h5>



                <!------------------------------------------------------------------------------------------------->
            </div>
            <div class="col-md-3"></div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-header">
                        <span class="letraNaranja">Publicaciones</span>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title letraBlanca">10,233</h5>
                    </div>
                    <div class="card-footer text-muted">
                        Hace 2 Días
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-header">
                        <span class="letraNaranja">Compras</span>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title letraBlanca">5,564</h5>
                    </div>
                    <div class="card-footer text-muted">
                        Hace 2 Días
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-header">
                        <span class="letraNaranja">Ventas</span>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title letraBlanca">3,234</h5>
                    </div>
                    <div class="card-footer text-muted">
                        Hace 2 Días
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Foto mas vendida</h5>
                        <p class="card-text letraNaranja">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis molestie ultrices. Ut nec rhoncus risus, vitae dictum ante. Nullam sem orci, ultrices et lobortis id, mollis luctus libero. Etiam vel dapibus ligula. Duis non fringilla est. Praesent mi quam, laoreet eu tortor ac, pellentesque finibus nisl. Praesent fringilla magna eu consectetur ultrices. Donec vulputate tellus mattis, mattis dui quis, mollis ante. Aenean lacinia metus leo, eu luctus leo imperdiet ac. Sed vel odio vel eros commodo finibus ut vel libero. Sed ultrices convallis tincidunt. Vivamus lobortis rhoncus purus, nec accumsan magna egestas a. Phasellus vel augue lobortis, vulputate lectus sit amet, vehicula neque. Suspendisse sollicitudin nec tortor eu pretium. Maecenas at tincidunt nunc, id feugiat felis. In faucibus nec lectus et maximus.</p>
                        <p class="card-text letraNaranja" "><small class="text-muted">Ultima actualizacion 3 min</small></p>
                    </div>
                    <img class="card-img-bottom" src="<?= ($ruta) ?>img/fondoLogin3.jpg" alt="Card image cap">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Mas me gusta</h5>
                        <p class="card-text letraNaranja">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis molestie ultrices. Ut nec rhoncus risus, vitae dictum ante. Nullam sem orci, ultrices et lobortis id, mollis luctus libero. Etiam vel dapibus ligula. Duis non fringilla est. Praesent mi quam, laoreet eu tortor ac, pellentesque finibus nisl. Praesent fringilla magna eu consectetur ultrices. Donec vulputate tellus mattis, mattis dui quis, mollis ante. Aenean lacinia metus leo, eu luctus leo imperdiet ac. Sed vel odio vel eros commodo finibus ut vel libero. Sed ultrices convallis tincidunt. Vivamus lobortis rhoncus purus, nec accumsan magna egestas a. Phasellus vel augue lobortis, vulputate lectus sit amet, vehicula neque. Suspendisse sollicitudin nec tortor eu pretium. Maecenas at tincidunt nunc, id feugiat felis. In faucibus nec lectus et maximus.</p>
                        <p class="card-text letraNaranja" "><small class="text-muted">Ultima actualizacion 3 min</small></p>
                    </div>
                    <img class="card-img-bottom" src="<?= ($ruta) ?>img/fondoLogin.jpg" alt="Card image cap">
                </div>
            </div>
        </div>
        <br />
        <center><h1>Actividades Recientes</h1></center>
        <div class="row">
            <div class="jumbotron">
                <h1 class="display-4">No se encuentra actividad!</h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis molestie ultrices. Ut nec rhoncus risus, vitae dictum ante. Nullam sem orci, ultrices et lobortis id, mollis luctus libero. Etiam vel dapibus ligula. Duis non fringilla est. Praesent mi quam, laoreet eu tortor ac, pellentesque finibus nisl.</p>
                <hr class="my-4">
                <!--<p>It uses utility classes for typography and spacing to space content out within the larger container.</p>-->
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="#" role="button">Leer mas</a>
                </p>
            </div>
        </div>
    </div>
    <footer class="page-footer font-small mdb-color lighten-3 pt-4" style="margin:0em; padding:0em; background-color:gray;">

        <!-- Footer Elements -->
        <div class="container">

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-lg-2 col-md-12 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-2 col-md-6 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(78).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-2 col-md-6 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(79).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-2 col-md-12 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(81).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-2 col-md-6 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(82).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-2 col-md-6 mb-4">

                    <!--Image-->
                    <div class="view overlay z-depth-1-half">
                        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(84).jpg" class="img-fluid"
                             alt="">
                        <a href="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->


        </div>
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3" style="background-color:#545454; margin:0em; padding:0em;">
            <span class="letraBlanca">© 2018 Copyright:</span>
            <a href="https://www.Deltadevs.com"> Deltadevs.com</a>
        </div>
        <!-- Copyright -->

    </footer>

</body>
</html>
