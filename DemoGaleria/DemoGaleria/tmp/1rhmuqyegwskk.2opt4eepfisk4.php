﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Pagina Principal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <link href="<?= ($ruta) ?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/deltadev.css" rel="stylesheet" />

</head>
<body id="principal">

    <nav class="navbar navbar-expand-lg " id="menu">
        <a class="navbar-brand letraBlanca" href="#">PHOTO NEWS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link letraNaranja" href="#">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link letraNaranja" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link letraNaranja" href="#">Ilustraciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link letraNaranja" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Contacto
                    </a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar">
                <button type="button" class="btn btn-secondary">Buscar</button>
            </form>

            <form class="form-inline my-2 my-lg-0">
                <!-- <button class="btn btn-outline-success my-2 my-sm-0 espacio" type="submit">Iniciar Sesi&oacute;n</button>-->
                <button type="button" class="btn btn-info espacio" onclick="location.href='<?= ($ruta) ?>iniciarsesion';">Iniciar Sesi&oacute;n</button>
            </form>
        </div>
    </nav>
    <br />
    <!-- Photo Grid -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner tama">
            <div class="carousel-item active">
                <img class="d-block w-100 flo" src="img/fondo.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Premer slider</h5>
                    <p>...</p>
                </div>
            </div>
            <div class="carousel-item tama">
                <img class="d-block w-100 flo" src="img/fondo.jpg" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Segundo slider</h5>
                    <p>...</p>
                </div>
            </div>
            <div class="carousel-item tama">
                <img class="d-block w-100 flo" src="img/fondo.jpg" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Tercer Slider</h5>
                    <p>...</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row">
        <div class="column ex1">
            <a data-toggle="modal" data-target="#modal1"><img src="img/img25.jpg"></a>
            <a data-toggle="modal" data-target="#modal2"><img src="img/img26.jpg"></a>
            <a data-toggle="modal" data-target="#modal3"><img src="img/img3.jpg"></a>
            <a data-toggle="modal" data-target="#modal4"><img src="img/img4.jpg"></a>
            <a data-toggle="modal" data-target="#modal5"><img src="img/img5.jpg"></a>
        </div>
        <div class="column">
            <a data-toggle="modal" data-target="#modal6"><img src="img/img6.jpg"></a>
            <a data-toggle="modal" data-target="#modal7"><img src="img/img7.jpg"></a>
            <a data-toggle="modal" data-target="#modal8"><img src="img/img8.jpg"></a>
            <a data-toggle="modal" data-target="#modal9"><img src="img/img9.jpg"></a>
            <a data-toggle="modal" data-target="#modal10"><img src="img/img10.jpg"></a>
            <a data-toggle="modal" data-target="#modal11"><img src="img/img11.jpg"></a>
        </div>
        <div class="column">
            <a data-toggle="modal" data-target="#modal12"><img src="img/img12.jpg"></a>
            <a data-toggle="modal" data-target="#modal13"><img src="img/img13.jpg"></a>
            <a data-toggle="modal" data-target="#modal14"><img src="img/img27.jpg"></a>
            <a data-toggle="modal" data-target="#modal15"><img src="img/img28.jpg"></a>
            <a data-toggle="modal" data-target="#modal16"><img src="img/img30.jpg"></a>
        </div>
        <div class="column">
            <a data-toggle="modal" data-target="#modal17"><img src="img/img17.jpg"></a>
            <a data-toggle="modal" data-target="#modal18"><img src="img/img18.jpg"></a>
            <a data-toggle="modal" data-target="#modal19"><img src="img/img19.jpg"></a>
            <a data-toggle="modal" data-target="#modal20"><img src="img/img20.jpg"></a>
            <a data-toggle="modal" data-target="#modal21"><img src="img/img29.jpg"></a>
            <a data-toggle="modal" data-target="#modal22"><img src="img/img22.jpg"></a>
        </div>
    </div>


    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img25.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img26.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img3.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img4.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img5.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img6.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img7.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img8.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal9" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img9.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img10.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img11.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal12" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img12.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal13" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img13.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal14" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img27.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal15" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img28.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal16" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img30.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal17" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img17.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal18" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img18.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal19" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img19.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal20" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img20.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal21" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img29.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal22" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Titulo De la foto</h5>
                </div>
                <div class="modal-body">
                    <img class="tam" src="img/img22.jpg">
                    <br />
                    <br />
                    <br />
                    <form class="form-inline my-2 my-lg-0">
                        <h3>$100.00</h3>
                        <h3 class="letraBlanca">p</h3>
                        <button type="button" class="btn btn-success">Comprar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= ($ruta) ?>js/jquery-3.3.1.js"></script>
    <script src="<?= ($ruta) ?>js/bootstrap.js"></script>
    <script src="<?= ($ruta) ?>js/bootstrap.min.js"></script>
    <script src="<?= ($ruta) ?>js/deltadevs.js"></script>
</body>
</html>