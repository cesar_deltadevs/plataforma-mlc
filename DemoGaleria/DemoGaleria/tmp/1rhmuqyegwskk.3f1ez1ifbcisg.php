﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Noticias</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <link href="<?= ($ruta) ?>css/bootstrap.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= ($ruta) ?>css/deltadev.css" rel="stylesheet" />

</head>
<body id="principal">
    <nav class="navbar navbar-dark " style="background-color:white; margin:0em;">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" style="background-color:white; color:black; font-size:120%;" aria-label="Toggle navigation">
            <img src="img/menu_50px.png" style="width:20%;" />&nbsp;NAVEGAR
        </button>
        <button type="button" class="btn " style="background-color:white; font-size:120%;" onclick="location.href='<?= ($ruta) ?>paquetes';">PRECIOS</button>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">asdasjdkasdj</p>
        <img src="img/LOGOPHOTONEWS.png" class="justify-content-md-center" style="width:25%;" />
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <p style="color:white;">.</p>
        <button type="button" class="btn " style="background-color:white; font-size:120%;"><img src="img/board_50px.png" style="width:20%;" />BOARDS</button>
        <button type="button" class="btn " style="background-color:white; font-size:120%;" onclick="location.href='<?= ($ruta) ?>carrito';"><img src="img/carrito_50px.png" style="width:20%;" />&nbsp;CARRITO</button>
        <button type="button" class="btn btn-outline-dark" style=" font-size:120%;" onclick="location.href='<?= ($ruta) ?>iniciarsesion';">Iniciar Sesión</button>

        <div class="collapse navbar-collapse" id="navbarsExample01">
            <ul class="navbar-nav mr-auto" style="color:black;">
                <li class="nav-item active">
                    <a class="" style="color:black; font-size:120%;" href="<?= ($ruta) ?>">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color:black; font-size:120%;" href="#">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" style="color:black; font-size:120%;" href="#">Board</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link disabled" style="color:black; font-size:120%;" href="#">Carrito</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="">
        <div class="row" style="margin:0em;">
            <div class="jumbotron" style="margin:0em; padding:1em; width:100%; background-color:white;">
                <div class="container">
                    <div class="input-group lu" style=" width: 70%;margin-left: auto;margin-right: auto;">
                        <input type="text" class="form-control" placeholder="Busca las mejores fotos e imágenes creativas" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button type="button" class="form-control" style="background-color:white;"><img src="img/carama_50px.png" style="width:40%" /></button>
                        </div>
                        <div class="input-group-append">
                            <select class="form-control">
                                <option>PAÍS</option>
                                <option>...</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <h4 style="margin:0em 0em 0em 5em">NOTICIAS</h4>
        <div class="row" style="margin:2em; padding:1em;">
            <div class="col-md-4" >
                <a href="#"><img src="img/img12.jpg" style="width:100%; " /></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus odio id lacus suscipit efficitur at laoreet urna. Sed scelerisque, diam sit amet vestibulum scelerisque, odio tellus posuere nisi, sit amet lacinia neque orci in mi. Ut et nisl bibendum orci congue elementum ultricies a lacus. </p>
            </div>
            <div class="col-md-4">
                <a href="#"><img src="img/img17.jpg" style="width:100%; " /></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus odio id lacus suscipit efficitur at laoreet urna. Sed scelerisque, diam sit amet vestibulum scelerisque, odio tellus posuere nisi, sit amet lacinia neque orci in mi. Ut et nisl bibendum orci congue elementum ultricies a lacus. </p>
            </div>
            <div class="col-md-4">
                <a href="#"><img src="img/img20.jpg" style="width:100%;" /></a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dapibus odio id lacus suscipit efficitur at laoreet urna. Sed scelerisque, diam sit amet vestibulum scelerisque, odio tellus posuere nisi, sit amet lacinia neque orci in mi. Ut et nisl bibendum orci congue elementum ultricies a lacus. </p>
            </div>
        </div>
        <footer class="page-footer font-small blue">

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">
                <img src="img/logo_Canon_fondo_BCO.png" style="width:10%" />
                <img src="img/ZENIT_copia.png" style="width:10%" />
                <img src="img/logoprintspot.png" style="width:7%" />
                <img src="img/logocanonacademy2.png" style="width:7%" />
            </div>
            <!-- Copyright -->

        </footer>
    </div>
    <script src="<?= ($ruta) ?>js/jquery-3.3.1.js"></script>
    <script src="<?= ($ruta) ?>js/bootstrap.js"></script>
    <script src="<?= ($ruta) ?>js/bootstrap.min.js"></script>
    <script src="<?= ($ruta) ?>js/deltadevs.js"></script>
</body>
</html>