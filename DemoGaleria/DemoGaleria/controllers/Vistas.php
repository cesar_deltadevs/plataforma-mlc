<?php

namespace controllers
{
	/**
	 * Usuarios short summary.
	 *
	 * Usuarios description.
	 *
	 * @version 1.0
	 * @author ccq
	 */
	class Vistas
	{
        public function VistaPrincipal($f3) {
            //echo \View::instance()->render('principal.html');

            echo \Template::instance()->render('principal.html');
        }
        public function VistaPrincipal2($f3) {
            //echo \View::instance()->render('principal.html');

            echo \Template::instance()->render('Principal2.html');
        }
        public function VistaLogin($f3){
            echo \Template::instance()->render("login.html");
        }
        public function VistaUsuarios($f3){
             echo \Template::instance()->render("Usuarios.html");
            //echo \View::instance()->render('login.html', 'text/html', array(
            //        "ruta" => $f3->get('ruta')
            //    ));

        }
        public function VistaNoticias($f3){
             echo \Template::instance()->render("noticias.html");
            //echo \View::instance()->render('login.html', 'text/html', array(
            //        "ruta" => $f3->get('ruta')
            //    ));

        }
        public function VistaFotos($f3){
             echo \Template::instance()->render("fotos.html");

        }
        public function VistaCarrito($f3){
             echo \Template::instance()->render("carrito.html");

        }
        public function VistaCampra($f3){
             echo \Template::instance()->render("compra.html");

        }
        public function VistaDatosCompra($f3){
             echo \Template::instance()->render("datosCompra.html");

        }
        public function VistaPaquetes($f3){
             echo \Template::instance()->render("paquetes.html");

        }

	}
}