<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG', 3);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');
if($f3->get('DEBUG') == 0) {
    $f3->set('ruta', 'http://' . $f3->get('HOST') . '/photonews/');
}
else {
    $f3->set('ruta', 'http://' . $f3->get('HOST') . ':' . $f3->get('PORT') . '/');
}

//if ($f3->get('DEBUG') > 0){
//    //Obtiene la ruta del sitio si esta en DEBUG
//    $f3->set('ruta', 'http://' . $f3->get('HOST') . ':' . $f3->get('PORT') . '/');
//} else{
//    //Obtiene la ruta del sitio si no esta en DEBUG
//    $f3->set('ruta', 'http://' . $f3->get('HOST') . '/v2/');
//}

define ('APPPATH', __DIR__);

################Rutas##################
#Rutas para las vistas
$f3->route('GET /', 'controllers\Vistas->VistaPrincipal2');
$f3->route('GET /iniciarsesion', 'controllers\Vistas->VistaLogin');
$f3->route('GET /usuario', 'controllers\Vistas->VistaUsuarios');
$f3->route('GET /noticias', 'controllers\Vistas->VistaNoticias');
$f3->route('GET /fotos', 'controllers\Vistas->VistaFotos');
$f3->route('GET /carrito', 'controllers\Vistas->VistaCarrito');
$f3->route('GET /compra', 'controllers\Vistas->VistaCampra');
$f3->route('GET /datos-compra', 'controllers\Vistas->VistaDatosCompra');
$f3->route('GET /paquetes', 'controllers\Vistas->VistaPaquetes');

$f3->run();